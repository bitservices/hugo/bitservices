# Themes

Themes should be linked to this folder by using Git submodules.

Example:

```
git submodule add "https://github.com/pacollins/hugo-future-imperfect-slim.git" "themes/hugo-future-imperfect-slim"
```
