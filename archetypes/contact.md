---
title            : "Contact"
type             : "contact"
date             : {{ .Date }}
layout           : contact
netlify          : false
emailservice     : "flipmail.co/api/YOUR_API_KEY_HERE"
emailsubject     : "A message from your websites contact form"
contactname      : "Your Name"
contactemail     : "Your Email Address"
contactmessage   : "Your Message"
contactanswertime: 24
draft            : true
---

## Contact Us
