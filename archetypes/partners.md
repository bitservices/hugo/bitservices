---
title      :   "{{ replace .TranslationBaseName "-" " " | title }}"
type       :   "post"
date       :   {{ .Date }}
description:   "Partner description"
categories : [ "Partners" ]
author     :   "Partner"
format     :   "Partnership Type"
link       :   "https://"
linktitle  :   "Partners Website"
draft      :   true
---

## Partner
