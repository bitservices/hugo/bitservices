---
title      :   "{{ replace .TranslationBaseName "-" " " | title }}"
type       :   "post"
date       :   {{ .Date }}
description:   "Project description"
categories : [ "Projects" ]
author     :   "Project"
format     :   "Project Title"
link       :   "https://"
linktitle  :   "Client Website"
draft      :   true
---

## Project
