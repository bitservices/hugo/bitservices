---
title: "Home"
type : "home"
date : 2017-11-12T12:38:32Z
---

# Introduction
BITServices offer consultancy and engineering expertise in cloud computing,
infrastructure automation and software development. Essentially, BITServices is
all things what many refer to as DevOps.

# Projects
Please see the "[Projects](/projects/)" page
for more details about projects we have carried out or have played a major
part.

# Technologies
In modern I.T. technologies change very rapidly. We have the passion to
continually adapt and learn what is up and coming. Whatever technology stack
your organisation has chosen, we will be able to adapt to deliver. Please find
below some of the technologies we have used to deliver projects so far:

## Cloud

{{< figure caption="Amazon Web Services" src="/img/home/aws.png" alt="Amazon Web Services Logo" link="https://aws.amazon.com/" target="_blank" class="technologies" >}}
{{< figure caption="Microsoft Azure" src="/img/home/azure.png" alt="Microsoft Azure Logo" link="https://azure.microsoft.com/" target="_blank" class="technologies" >}}

## Containerisation

{{< figure caption="Kubernetes" src="/img/home/kubernetes.png" alt="Kubernetes Logo" link="https://kubernetes.io/" target="_blank" class="technologies" >}}
{{< figure caption="Docker" src="/img/home/docker.png" alt="Docker Logo" link="https://docker.io/" target="_blank" class="technologies" >}}

## Hashicorp Stack

{{< figure caption="Terraform" src="/img/home/terraform.png" alt="Terraform Logo" link="https://www.terraform.io/" target="_blank" class="technologies" >}}
{{< figure caption="Packer" src="/img/home/packer.png" alt="Packer Logo" link="https://www.packer.io/" target="_blank" class="technologies" >}}
{{< figure caption="Vagrant" src="/img/home/vagrant.png" alt="Vagrant Logo" link="https://www.vagrantup.com/" target="_blank" class="technologies" >}}
{{< figure caption="Consul" src="/img/home/consul.png" alt="Consul Logo" link="https://www.consul.io/" target="_blank" class="technologies" >}}
{{< figure caption="Vault" src="/img/home/vault.png" alt="Vault Logo" link="https://www.vaultproject.io/" target="_blank" class="technologies" >}}

## Continuous Integration & Deployment

{{< figure caption="Gitlab" src="/img/home/gitlab.png" alt="Gitlab Logo" link="https://gitlab.com/" target="_blank" class="technologies" >}}
{{< figure caption="ArgoCD" src="/img/home/argocd.png" alt="ArgoCD Logo" link="https://argoproj.github.io/cd/" target="_blank" class="technologies" >}}
{{< figure caption="Azure DevOps" src="/img/home/azure-devops.png" alt="Azure DevOps Logo" link="https://azure.microsoft.com/services/devops/" target="_blank" class="technologies" >}}
{{< figure caption="Jenkins" src="/img/home/jenkins.png" alt="Jenkins Logo" link="https://www.jenkins.io/" target="_blank" class="technologies" >}}

## Linux

{{< figure caption="Debian" src="/img/home/debian.png" alt="Debian Logo" link="https://www.debian.org/" target="_blank" class="technologies" >}}
{{< figure caption="Ubuntu" src="/img/home/ubuntu.png" alt="Ubuntu Logo" link="https://ubuntu.com/" target="_blank" class="technologies" >}}
{{< figure caption="Red Hat" src="/img/home/redhat.png" alt="Red Hat Logo" link="https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux" target="_blank" class="technologies" >}}
{{< figure caption="CentOS" src="/img/home/centos.png" alt="CentOS Logo" link="https://www.centos.org/" target="_blank" class="technologies" >}}
{{< figure caption="Arch" src="/img/home/arch.png" alt="Arch Logo" link="https://www.archlinux.org/" target="_blank" class="technologies" >}}

# Partners
To help us offer the best service possible to our clients, we have established
partnerships with other specialist organisations. These partnerships give us
access to knowledge around various platforms we often work with. Also, where
appropiate, we also work with some of our partners when delivering a project.
For more details, please see the "[Partners](/partners/)" page.
