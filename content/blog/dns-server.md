---
title     : "The BITServices Public DNS Service"
type      : "post"
date      : 2025-03-04T12:54:00+00:00
author    : Richard Lees
categories: [ "DNS", "IPv6", "AWS", "Packer" ]
---
# DNS Service? What DNS Service?

Whilst it has never been announced or details ever posted, BITServices does host
a fully public DNS over TLS, DNS over HTTPS and DNS over QUIC service. The
service was intended for internal use mainly. When trying to use such a service
when out and about however, it really needs to be fully open to the public
Internet.

# About the BITServices DNS Service

The DNS service is as simple as possible and it only serves a single purpose: to
adblock based on [Steven Black's blocklist](https://github.com/StevenBlack/hosts).

Any request that is not blocked is forwarded (over TLS) to [CloudFlare's 1.1.1.1](https://one.one.one.one/).

# Describe Public?

The article about the public DNS server seems to be missing something obvious,
the actual service address.

Well it was never meant to go public in that way. It was meant for internal use
mainly so the addresses were never handed out. There was however an expectation
that people may stumble across it and use it. As long as the current hosting
size can handle the load, it is absolutely fine.

Recently, it seems the addresses have been discovered (I am not posting where).
This has led to a noticeable uptick in use. Whilst everything is still well
within limits today, it will be stated here that if that changes, network
blocking will be implemented.

It also means that seemingly silly changes can be implemented at will, at any
time, without warning. With that in mind...

# IPv6

... in the near future, the DNS service will be removed from the IPv4 Internet
entirely and will be accessible by IPv6 only.

This is NOT a blocking measure. The service is hosted on [AWS] and they have now
implement costs for IPv4 addresses. As an IPv6 evangelist myself, every use case
I have for the DNS service now has access to the IPv6 Internet. With that in
mind, given the purpose of the DNS service, it makes sense to reduce cloud costs.

Almost all random traffic to the DNS service appears to be IPv4. This post is
mainly to help anyone who wonders what happened to the DNS service when the move
to IPv6 only happens.

# Traffic is IPv4? How do you know?!

So this might raise questions around logging. The short answer is that NO, the
DNS servers do NOT log requests.

That said, it is not a promise, or a guarantee. When messing around,
trying stuff, etc, logging can be temporarily enabled to assist with this. There is no
development environments, so all messing around is carried out in production.
The last time I happened to do this, I noticed a LOT of IPv4 traffic.

# I Want One!

Want your own ad blocking encrypted DNS server?

## Packer

The [Packer](https://packer.io/) repository is fully open and this builds an
[AWS] image to launch the DNS server from.

[dns-aws](https://gitlab.com/bitservices/packer/dns-aws/)

## Certificates

To set up TLS, you'd need to use something like [CertBot](https://certbot.eff.org/),
which is easy if you run a single server, but more interesting if you run a
couple (or more).

I use these helper scripts: [bitservices-letsencrypt](https://gitlab.com/bitservices/archlinux/bitservices-letsencrypt)

<!----------------------------------------------------------------------------->

[AWS]: https://aws.amazon.com/
