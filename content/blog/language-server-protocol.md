---
title     : "Language Server Protocol (LSP)"
type      : "post"
date      : 2023-02-22T16:15:00+01:00
author    : Richard Lees
categories: [ "LSP", "Development" ]
---
# Introduction

One really underused feature in platform engineering in my experience is the
[Language Server Protocol] (LSP).

More traditional software developers may not get as much value, as they tend to
use more full fat Integrated Development Environments (IDEs) such as
[Visual Studio], [IntelliJ IDEA] or [PhpStorm] depending on the specific
language a project is developed in.

Platform engineers (or 'DevOps' engineers) tend to use more lightweight editors
due to the amount of different languages and/or markups they need to work with
often within the same project. Many of these languages, such as [HCL] for
developing infrastructure as code in [Terraform], do not have a corresponding
full fat IDE. Platform engineers would normally use something like
[Visual Studio Code] or [Vim].

LSP enables lightweight editors to get full fat IDE features for many different
languages and even markup languages such as [YAML]. Since LSP decouples the
editor from the language tools it allows you to get the same rich features no
matter what your editor of choice is (as long as it supports LSP!).

Since I have spent many years living in [Terraform], [YAML], shell scripts, etc
the prospect of having features such as code completion, diagnostics and
automatic formatting for these languages in my favourite editor was very
exciting!

# Language Server Protocol Overview

LSP was originally developed by Microsoft.

* Official LSP page: [https://microsoft.github.io/language-server-protocol/](https://microsoft.github.io/language-server-protocol/)
* List of Language Servers: [https://langserver.org/](https://langserver.org/)

## Without Language Server Protocol

{{< figure caption="Without Language Server Protocol" src="/img/language-server-protocol/nolsp.png" alt="Without Language Server Protocol diagram" >}}

Each IDE needed its own code processing engine in order to offer rich development features.

## With Language Server Protocol

{{< figure caption="With Language Server Protocol" src="/img/language-server-protocol/lsp.png" alt="With Language Server Protocol diagram" >}}

Many different editors can use multiple stand-alone code processing engines that can be maintained seperately.

# Examples

In these example [Neovim] is used.

{{< collapsable "Go" >}}
{{< figure caption="Go with Language Server Protocol" src="/img/language-server-protocol/golsp.gif" alt="Go with Language Server Protocol example" >}}
{{< figure caption="Go without Language Server Protocol" src="/img/language-server-protocol/gonolsp.gif" alt="Go without Language Server Protocol example" >}}
{{< /collapsable >}}

{{< collapsable "Bicep" >}}
{{< figure caption="Bicep with Language Server Protocol" src="/img/language-server-protocol/biceplsp.gif" alt="Bicep with Language Server Protocol example" >}}
{{< figure caption="Bicep without Language Server Protocol" src="/img/language-server-protocol/bicepnolsp.gif" alt="Bicep without Language Server Protocol example" >}}
{{< /collapsable >}}

{{< collapsable "Powershell" >}}
{{< figure caption="Powershell with Language Server Protocol" src="/img/language-server-protocol/pwshlsp.gif" alt="Powershell with Language Server Protocol example" >}}
{{< figure caption="Powershell without Language Server Protocol" src="/img/language-server-protocol/pwshnolsp.gif" alt="Powershell without Language Server Protocol example" >}}
{{< /collapsable >}}

{{< collapsable "YAML" >}}
{{< figure caption="YAML with Language Server Protocol" src="/img/language-server-protocol/yamllsp.gif" alt="YAML with Language Server Protocol example" >}}
{{< figure caption="YAML without Language Server Protocol" src="/img/language-server-protocol/yamlnolsp.gif" alt="YAML without Language Server Protocol example" >}}
What is particularly impressive with the YAML language server is that it can
tell you are working with certain types of files and can do code completetion
and diagnostics against a known schema. In this example, a Gitlab CI file is
used and you can see that the Language Server is working with a schema for
Gitlab CI.
{{< /collapsable >}}

# Closing Notes

Since using LSP the amount of bad commits I made has been drastically reduced
since I can see syntax and schema errors infront of me as I make them. Since
formatting is also taken care of automatically this reduces excess commits
later on during the pull request process.

I do not use VSCode so sadly do not know how to get LSP going there if it does
not do it out the box. A cursory search reveals little. I am certain that
VSCode does support LSP.

I am not sure if [Vim] supports LSP natively yet. I personally use [Neovim]
which does support LSP natively. The following link has details that help
setting up [Neovim]: [https://github.com/neovim/nvim-lspconfig](https://github.com/neovim/nvim-lspconfig).

Often people have compared LSP to [Github Copilot]. In my view they are very
different things. Copilot can use AI to suggest code and even entire functions.
LSP on the other hand provides direct language specific tooling. LSP will show
suggestions based on what methods, variables, etc are available whereas Copilot
will try and predict what you are trying to code by for example the name given
to a function.

<!---------------------------------------------------------------------------->

[HCL]:                      https://github.com/hashicorp/hcl
[Vim]:                      https://www.vim.org/
[YAML]:                     https://yaml.org/
[Neovim]:                   https://neovim.io/
[PhpStorm]:                 https://www.jetbrains.com/phpstorm/
[Terraform]:                https://www.terraform.io/
[IntelliJ IDEA]:            https://www.jetbrains.com/idea/
[Visual Studio]:            https://visualstudio.microsoft.com/
[Github Copilot]:           https://github.com/features/copilot
[Visual Studio Code]:       https://code.visualstudio.com/
[Language Server Protocol]: https://microsoft.github.io/language-server-protocol/
