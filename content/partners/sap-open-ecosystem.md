---
title      :   "SAP"
type       :   "post"
date       :   2017-12-12T21:43:04Z
description:   "SAP Open Ecosystem partnership"
categories : [ "Partners", "SAP", "SAP Commerce" ]
author     :   "Partner"
---

[![SAP PartnerEdge Open Ecosystem](/img/sap-open-ecosystem/logo.png)](https://www.sap.com/uk/index.html)

BITServices Ltd have officially joined the
[SAP PartnerEdge Open Ecosystem](https://www.sap.com/partner/become.html).

This allows us access to specialist resources to further help us carry out
projects such as cloud migrations for our customers who use the
[SAP Commerce](https://www.sap.com/uk/products/crm/e-commerce-platforms.html)
e-commerce platform.
