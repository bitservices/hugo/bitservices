---
title      :   "Smartr365"
type       :   "post"
date       :   "2022-08-01"
description:   "Improvements to infrastructure for hosting multi-tenant Azure App Service"
categories : [ "Projects", "Bicep", "Powershell", "Azure DevOps", "Azure" ]
author     :   "Project"
---

[![Smartr365](/img/smartr365/logo.png)](https://www.smartr365.com/)

A part time project where the main goal was to implement [Azure Front Door]
into the existing infrastructure automation to improve security and
performance of an application with hundreds of tenants.

# Azure Front Door

Performance improvements were achieved by leveraging caching, compression and
the content delivery network to service static and cached content from regional
distribution points.

To improve security [Azure Front Door] [Web Application Firewall] (WAF) was
used to identify improvements in the application such as ways to filter/encode
data that was transmitted to/from the application and customers web browsers.
The WAF also enabled visibility to how the site was currently being
scraped/attacked and potential mitigation.

# Technologies Used

The existing infrastructure automation was primarily in [Bicep] and
[Powershell]. These technologies have not previously been used by BITServices
and therefore brought new knowledge to the company and contrast with the
alternative tools that we much more commonly use.

# Other Deliverables

As well as delivering [Azure Front Door], other small tasks carried out
included creating another environment with infrastructure as code which
required heavy refactoring. The refactoring was mainly converting scripted
[PowerShell] steps into proper infrastructure as code ([Bicep] in this case)
and fixing infrastructure deployment pipelines that were failing for various
reasons.

<!---------------------------------------------------------------------------->

[Bicep]:                    https://github.com/Azure/bicep
[PowerShell]:               https://github.com/PowerShell/PowerShell
[Azure Front Door]:         https://azure.microsoft.com/products/frontdoor/
[Web Application Firewall]: https://azure.microsoft.com/products/web-application-firewall/
