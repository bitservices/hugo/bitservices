---
title      :   "FoodFriends"
type       :   "post"
date       :   "2017-09-01"
description:   "Fully automated Wordpress platform hosted on Amazon Web Services"
categories : [ "Projects", "Hashicorp", "Terraform", "Ansible", "AWS", "Wordpress" ]
author     :   "Project"
---

![FoodFriends](/img/foodfriends/logo.png)

A small project carried out for a family member.

# The Problem

FoodFriends have a small [Wordpress] website that was hosted with a traditional
hosting provider. The provider, unfortunately had crippling limits such as only
50MiB of disk for website media. FoodFriends required that website stayed on
the [Wordpress] platform.

# The Solution

The website was moved to [Amazon Web Services].

To remove future support costs and time, the supporting infrastructure was
automated with Hashicorp [Terraform]. The installation and configuration of
software fully automated with [Ansible] - even down to the renewal of the TLS
certificate!

By using Amazon [S3] backups are automatically taken and rotated daily.
Restoration of data is managed by the [Ansible] playbooks meaning the entire
infrastructure can be built from nothing with all data restored in just
minutes.

# The Result

There is now gigabytes of available diskspace for media. As
[Amazon Web Services] is an enterprise grade platform capable of hosting even
the most busiest of e-commerce websites the performance of the FoodFriends
website has increased dramatically.

Having TLS configured correctly and being made mandatory the FoodFriends
website has had its ranking increased on [Google].

The current cost of hosting the infrastructure is approximately $1 a month for
a couple of [Route53] zones. Once the free-tier expires, the overall cost will
still be **only a third** of what it was with the previous host.

<!---------------------------------------------------------------------------->

[S3]:                  https://aws.amazon.com/s3/
[Google]:              https://www.google.com/
[Ansible]:             https://www.ansible.com/
[Route53]:             https://aws.amazon.com/route53/
[Terraform]:           https://terraform.io/
[Wordpress]:           https://wordpress.com/
[Amazon Web Services]: https://aws.amazon.com/
