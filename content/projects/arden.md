---
title      :   "Arden University"
type       :   "post"
date       :   "2022-01-01"
description:   "Migration of Virtual Learning Environment to Amazon Elastic Kubernetes Service (EKS)"
categories : [ "Projects", "Kubernetes", "EKS", "Docker", "Hashicorp", "Terraform", "AWS", "ArgoCD" ]
author     :   "Project"
---

[![Arden University](/img/arden-university/logo.png)](https://arden.ac.uk)

To be completed.

